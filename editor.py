# -*- coding: utf-8 -*-
'''
Bruno de Oliveira
'''

import os.path

class ManipuladorArquivo(object):
    
    def salvar(self, arquivo_editor, texto, caminho = None):
        
        if arquivo_editor and texto:
            
            if caminho:
                arq = open(caminho, 'w')
            else:
                arq = open(arquivo_editor.caminho, 'w')
                
            arq.write(texto)
            arq.close()
            
    def abrir(self, arquivo_editor):
        
        if os.path.isfile(arquivo_editor.caminho):
            
            arq = open(arquivo_editor.caminho, 'r')
            conteudo = arq.read()
            arq.close()
            
            return conteudo
        
        return None

class ArquivoDoEditor(object):
    
    def __init__(self):
        
        self.caminho = None
        self._modificado = False
        
    @property
    def modificado(self):
        
        return self._modificado
    
    @modificado.setter
    def modificado(self, status_boolean):
        
        self._modificado = status_boolean
            
class Editor(object):
    
    def __init__(self):
        
        self._arquivo_editor = ArquivoDoEditor()
        self.manipulador_arquivo = ManipuladorArquivo()
        
    def salvar_arquivo(self, texto, caminho=None):
        
        self.manipulador_arquivo.salvar(self._arquivo_editor, texto,
                                         caminho)
        
            
        
    def novo_arquivo(self):
        
        self._arquivo_editor = ArquivoDoEditor()
        
    def abrir_arquivo(self, caminho_arquivo):
        
        arquivo = ArquivoDoEditor()
        arquivo.caminho = caminho_arquivo
        
        conteudo = self.manipulador_arquivo.abrir(arquivo)
        self._arquivo_editor = arquivo
        
        return conteudo
    
    def status_arquivo_string(self):
        
        if self._arquivo_editor._modificado:
            
            return 'Modificado'
        
        return 'Não Modificado'
    
    @property
    def arquivo_modificado(self):
        
        return self._arquivo_editor.modificado
    
    @arquivo_modificado.setter
    def arquivo_modificado(self, status_boolean):
        
        if type(status_boolean) == bool:
            
            self._arquivo_editor.modificado = status_boolean
    
    @property        
    def caminho_arquivo(self):
        
        if self._arquivo_editor.caminho:
            
            return self._arquivo_editor.caminho
        
        return ''
    
    @caminho_arquivo.setter
    def caminho_arquivo(self, caminho):
        
        self._arquivo_editor.caminho = caminho
        
    def eh_arquivo_novo(self):
        
        return not bool(self._arquivo_editor.caminho)                
        
    
               
    
   
    
                
            
            
