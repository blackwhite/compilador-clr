'''
Bruno de Oliveira
'''

import sys
from janela import JanelaPrincipal
from PySide import QtGui

app = QtGui.QApplication(sys.argv)

interface = JanelaPrincipal()
interface.show()

sys.exit(app.exec_())

