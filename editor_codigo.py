# -*- coding: utf-8 -*-
'''
Bruno de Oliveira
'''

from PySide.QtGui import QWidget, QPlainTextEdit, QPainter
from PySide.QtGui import  QTextEdit, QTextFormat, QApplication 
from PySide.QtCore import Qt, QRect, QSize
from config import cores

class NumeroLinhas(QWidget):
    
    def __init__(self, editor_codigo, *args):
        
        QWidget.__init__(self, editor_codigo)
        
        self.editor_codigo = editor_codigo
        
        
    def sizeHint(self):
        
        return QSize(self.editor_codigo.calcular_largura_numero_linha(),
                                   0)
        
    def paintEvent(self, event):
        
        
        self.editor_codigo.desenhar_numeracao_linha(event)
        

class EditorCodigo(QPlainTextEdit):
    
    def __init__(self, *args):
        
        QPlainTextEdit.__init__(self, *args)
        
        self.mostrar_num_linhas = NumeroLinhas(self)
              
        
        self.blockCountChanged.connect(self._editor_com_nada)
        self.blockCountChanged.connect(self.atualizar_largura_numero_linhas)
        self.updateRequest.connect(self.atualizar_area_numero_linhas)
        self.cursorPositionChanged.connect(self.colorir_linha_atual)
        
        self._editor_com_nada()
           
    def _resetar(self):
        '''
        Para resolver problema de espaçamento quando não existe nenhum
        bloco de texto
        '''
        
        self.setPlainText('\n\n')
    
        
    def _editor_com_nada(self):
        
        if self._precisa_resetar:
            
            self._resetar()
            
    @property    
    def _precisa_resetar(self):
        
        if self.blockCount() == 1:
            
            return True
        
        return False        
    
                  
    def atualizar_largura_numero_linhas(self, contagem_bloco = None):
              
        
        self.setViewportMargins(self.calcular_largura_numero_linha(), 0,
                                0, 0)
        
    def colorir_linha_atual(self):
        
        selecoes_extra = []
        
        if not self.isReadOnly():
            
            selecao = QTextEdit.ExtraSelection()
            cor_linha = cores['linha_atual']
            
            selecao.format.setBackground(cor_linha)
            selecao.format.setProperty(QTextFormat.FullWidthSelection,
                                       True)
            selecao.cursor = self.textCursor()
            selecao.cursor.clearSelection()
            selecoes_extra.append(selecao)
            
        self.setExtraSelections(selecoes_extra)
        
    def desenhar_numeracao_linha(self, event):
        
        pintor = QPainter(self.mostrar_num_linhas)
        pintor.fillRect(event.rect(), cores['fundo_numeracao_linha'])
        
        bloco_texto = self.firstVisibleBlock()
        numero_bloco = bloco_texto.blockNumber()
        
        top = int(self.blockBoundingGeometry(bloco_texto).translated(
                                                                    self.contentOffset()).top())
        
        bottom = top + int(self.blockBoundingRect(bloco_texto).height())
                                       
        while top <= event.rect().bottom():
            
            if bottom >= event.rect().top():
                
                num = numero_bloco + 1
                pintor.setPen(cores['digito_numeracao_linha'])
                pintor.drawText(0, top, self.mostrar_num_linhas.width(),
                                self.fontMetrics().height(), Qt.AlignLeft,
                                str(num))
                         
            top = bottom
            
            bottom = top + int(self.blockBoundingRect(bloco_texto).height())
            numero_bloco += 1 
    
    def showEvent(self, event):
        
        QPlainTextEdit.showEvent(self, event)
        self.atualizar_largura_numero_linhas()
        
            
        
    def calcular_largura_numero_linha(self):
        '''
        Calcular largura do widget
        '''
        
        digito = 1
        
        qtd_blocos_visiveis = int(self.height() / 17.0)
        qtd_blocos = self.blockCount()
               
        
        if qtd_blocos > qtd_blocos_visiveis:
            
            valor_max = max(1, qtd_blocos)
            
        else:
            
            valor_max = max(1, qtd_blocos + qtd_blocos_visiveis)
        
        
        while(valor_max >= 10):
            
            valor_max /= 10
            digito += 1
            
        espaco = 3 + self.fontMetrics().widthChar('9') * digito
        
        return espaco        
        
    def atualizar_area_numero_linhas(self, rect, dy):
        
       
        if dy:
            self.mostrar_num_linhas.scroll(0, dy)
            
        else:
            self.mostrar_num_linhas.update(0, rect.y(),
                                           self.mostrar_num_linhas.width(),
                                           rect.height()
                                           )
            
        if rect.contains(self.viewport().rect()):
            
            self.atualizar_largura_numero_linhas(0)
            
    def resizeEvent(self, event):
        
        QPlainTextEdit.resizeEvent(self, event)
               
        cr = self.contentsRect()
        
        self.mostrar_num_linhas.setGeometry(QRect(cr.left(), cr.top(),
                                                  self.calcular_largura_numero_linha(),
                                                  cr.height()))

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    editor = EditorCodigo()
    editor.show()
    
    sys.exit(app.exec_())
            
    