# -*- coding: utf-8 -*-
'''
Bruno de Oliveira
'''

from PySide import QtGui, QtCore
from ui_editor import Ui_MainWindow
from editor import Editor
from editor_codigo import EditorCodigo

class JanelaPrincipal(QtGui.QMainWindow):
    
    def __init__(self, *args):
        
        QtGui.QMainWindow.__init__(self, *args)
        self.ui = None
        self.editor = Editor()        
        self.define_ui()        
        self.define_meus_sinais()
        
        self.setWindowTitle('Editor')
    
    def define_ui(self):
        
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
                
        self.ui.status_arquivo_label = QtGui.QLabel(self.ui.statusBar)
        self.ui.status_arquivo_label.setGeometry(QtCore.QRect(0,524,91, 17))
        self.ui.status_arquivo_label.setObjectName('status_arquivo_label')
        
               
        self.ui.caminho_arquivo_label = QtGui.QLabel(self.ui.statusBar)
        self.ui.caminho_arquivo_label.setGeometry(QtCore.QRect(0, 590, 91, 17))
        self.ui.caminho_arquivo_label.setObjectName('caminho_arquivo_label')
        
               
        self.ui.statusBar.addWidget(self.ui.status_arquivo_label, 0)
        self.ui.statusBar.addWidget(self.ui.caminho_arquivo_label,1)
        
        self.atualizar_barra_status(self.editor.status_arquivo_string(),
                                    self.editor.caminho_arquivo)
        
        self.ui.editor_codigo = EditorCodigo(self.ui.centralwidget)
        self.ui.editor_codigo.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.ui.editor_codigo.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.ui.editor_codigo.setLineWrapMode(QtGui.QPlainTextEdit.NoWrap)
        self.ui.editor_codigo.setObjectName("editor_codigo")
        self.ui.gridLayout.addWidget(self.ui.editor_codigo, 0, 0, 1, 1)
                    
    def define_meus_sinais(self):
        '''
        Define os sinais que nao foram gerados pelo Qt Designer
        
        '''
        
        self.ui.equipe_action.triggered.connect(self.mostrar_equipe)
        self.ui.gerar_cod_action.triggered.connect(self.gerar_codigo)
        self.ui.compilar_action.triggered.connect(self.compilar)
       
        self.ui.novo_arquivo_action.triggered.connect(self.ui.area_mensagem.clear)
        self.ui.novo_arquivo_action.triggered.connect(self.ui.editor_codigo.clear)
        self.ui.novo_arquivo_action.triggered.connect(self.novo_arquivo)
        
        self.ui.abrir_arquivo_action.triggered.connect(self.abrir_arquivo)      
        self.ui.editor_codigo.textChanged.connect(self.arquivo_modificado)
        self.ui.salvar_action.triggered.connect(self.salvar_arquivo)
        
        self.ui.copiar_action.triggered.connect(self.ui.editor_codigo.copy)
        self.ui.recortar_action.triggered.connect(self.ui.editor_codigo.cut)
        self.ui.colar_action.triggered.connect(self.ui.editor_codigo.paste)
    
    def novo_arquivo(self):
        
        self.editor.novo_arquivo()
        self.atualizar_barra_status(self.editor.status_arquivo_string(),
                                    self.editor.caminho_arquivo) 
        
    def abrir_arquivo(self):
        
        caminho_arquivo = QtGui.QFileDialog.getOpenFileName(self,
                                                         'Abrir arquivo')[0]
        
        if caminho_arquivo:
            
            conteudo = self.editor.abrir_arquivo(caminho_arquivo)
            self.ui.area_mensagem.clear()
            self.ui.editor_codigo.setPlainText(unicode(conteudo))
            
            self.editor.arquivo_modificado = False
            self.atualizar_barra_status(self.editor.status_arquivo_string(),
                                        caminho_arquivo)
            
    def atualizar_barra_status(self, modificado, caminho_arquivo):
        
        self.ui.caminho_arquivo_label.setText(unicode(caminho_arquivo))
       
        self.ui.status_arquivo_label.setText(unicode(modificado))
              
    def salvar_arquivo(self):
        
        if self.editor.eh_arquivo_novo():
            
            nome_salvar = QtGui.QFileDialog.getSaveFileName(self,
                                                        'Salvar arquivo ')[0]
                                                        
            if nome_salvar:
                
                self.editor.salvar_arquivo(self.ui.editor_codigo.toPlainText(),
                                           nome_salvar)
                self.editor.caminho_arquivo = nome_salvar
                
            else:
                return
        
        else:
            
            self.editor.salvar_arquivo(self.ui.editor_codigo.toPlainText(),
                                       self.editor.caminho_arquivo)
        
        self.ui.area_mensagem.clear()        
        self.editor.arquivo_modificado = False    
        self.atualizar_barra_status(self.editor.status_arquivo_string(),
                                    self.editor.caminho_arquivo)
                                                        
    def arquivo_modificado(self):
        
        self.editor.arquivo_modificado = True
        self.atualizar_barra_status(self.editor.status_arquivo_string(),
                                    self.editor.caminho_arquivo)
            
    def gerar_codigo(self):
        
        self.ui.area_mensagem.setPlainText(unicode('Geração de código ainda não foi' +
                                           ' implementada'))
    
    def compilar(self):
        
        self.ui.area_mensagem.setPlainText(unicode('Compilação de programas ainda não'+
                                           ' foi implementada.'))
    def mostrar_equipe(self):
               
        QtGui.QMessageBox.about(self, "Equipe", "Equipe: Bruno de Oliveira")
 
