'''
Created on 17/03/2012

@author: teste
'''

from PySide.QtGui import QColor

cores = {'fundo_numeracao_linha': QColor.lighter(QColor(130,181,241),150),
         'linha_atual': QColor.lighter(QColor(163,187,93),185),
         'digito_numeracao_linha': QColor(154,164,164)
         }